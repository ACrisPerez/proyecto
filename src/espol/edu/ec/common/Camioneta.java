/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andyc
 */
public class Camioneta extends Vehiculo {
    private String transmision;    
    int vidrios;
    
    /*INICIALIZACIÓN DEL CONSTRUCTOR DE LA SUBCLASE*/
    public Camioneta(String tip, String placa, String color, String tipo_Motor, String marca, String modelo, Double precio, String tipo_combustible, double recorrido, int año, String transmision, int vidrios) {
        super(tip, placa, color, tipo_Motor, marca, modelo, precio, tipo_combustible, recorrido, año);
        this.transmision = transmision;
        this.vidrios = vidrios;
    }
    

    
    
    
    public String getTransmision(){
        return this.transmision;
    }
    
    public int getVidrios(){
        return this.vidrios;
    }
    
    public void setTransmision(String transmision){
        this.transmision = transmision;
    }
    
    public void setVidrios(int vidrios){
        this.vidrios=vidrios;
    }
    
    @Override
    public ArrayList<Vehiculo> leerRegistros(String archivo){
        ArrayList <Vehiculo> regVehiculo = new ArrayList<>();
        /*Leyendo el archivo*/
        try(Scanner sc = new Scanner(new File(archivo))){
            /*Por cada salto de línea que encuentre sigue leyendo el archivo*/
            while(sc.hasNextLine()){
                /*Obtengo cada uno de los elementos del vehículo al separarlos por las comas*/
                String[] palabras = sc.nextLine().split(",");
                /*Recordando que cuando voy a tomar una palabra es String*/
                Vehiculo c = new Camioneta(palabras[0], palabras[1],palabras[2],palabras[3],palabras[4],palabras[5],Double.parseDouble(palabras[6]),palabras[7],Double.parseDouble(palabras[8]),Integer.parseInt(palabras[9]),palabras[10],Integer.parseInt(palabras[11]));
                /*Añado el nuevo vehículo a la lista*/
                regVehiculo.add(c);      
          }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        /*Mostrar la lista de registros actualizada*/
        return regVehiculo;
    }
    
    
    @Override
    public String toString(){
        return super.toString() + "," + this.transmision + "," + this.vidrios;
    }
}
