/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andyc
 */
public class Motocicleta extends Vehiculo{
    

    
    /*Constructor de clase moto*/
    
    public Motocicleta(String tip,String placa, String color, String tipo_Motor, String marca, String modelo, double precio, String tipo_combustible, double recorrido, int año) {
        super(tip, placa, color, tipo_Motor, marca, modelo, precio, tipo_combustible, recorrido, año);

    }
    
    @Override
    public ArrayList<Vehiculo> leerRegistros(String archivo){
        ArrayList <Vehiculo> regVehiculo = new ArrayList<>();
        /*Leyendo el archivo*/
        try(Scanner sc = new Scanner(new File(archivo))){
            /*Por cada salto de línea que encuentre sigue leyendo el archivo*/
            while(sc.hasNextLine()){
                /*Obtengo cada uno de los elementos del vehículo al separarlos por las comas*/
                String[] palabras = sc.nextLine().split(",");
                /*Recordando que cuando voy a tomar una palabra es String*/
                Vehiculo c = new Motocicleta(palabras[0],palabras[1],palabras[2],palabras[3],palabras[4],palabras[5],Double.parseDouble(palabras[6]),palabras[7],Double.parseDouble(palabras[8]),Integer.parseInt(palabras[9]));
                /*Añado el nuevo vehículo a la lista*/
                regVehiculo.add(c);      
          }
        }
        catch(Exception e){
            System.out.println("");
        }
        /*Mostrar la lista de registros actualizada*/
        return regVehiculo;
    }
    
}