/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andyc
 */

public class Vehiculo {
    /*Asignación de los atributos de la clase*/
    protected String tipoV;
    protected String placa;
    protected String modelo;
    protected String marca;
    protected String color;
    protected String tipo_motor;
    protected double precio;
    protected String tipo_combustible;
    protected double recorrido;
    protected int año;
    private ArrayList<Vehiculo> vehiculos;
    
    public Vehiculo(String tipo, String placa, String color,String tipo_Motor, String marca, String modelo, double precio, String tipo_combustible, double recorrido, int año ){
        this.tipoV = tipo;
        this.placa = placa;
        this.color = color;
        this.tipo_motor = tipo_Motor;
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
        this.tipo_combustible=tipo_combustible;
        this.recorrido = recorrido;
        this.año = año;
        
    }
    public String getTipo(){
        return this.tipoV;
    }
    
    /*Getters y Setters*/
    public String getPlaca(){
        return this.placa;
    }
    
    public String getColor(){
        return this.color;
       
    }
    
    public String getTipoM(){
        return this.tipo_motor;
    }
    
    public String getMarca(){
        return this.marca;
    }
    
    public String getModelo(){
        return this.modelo;
    }
    
    public Double getPrecio(){
        return this.precio;
    }
    
    public String getComb(){
        return this.tipo_combustible;
    }
    
    public double getRecorrido(){
        return this.recorrido;
    }
    
    public int getAño(){
        return this.año;
    }
    
    public ArrayList<Vehiculo> getListaVehic(){
        return this.vehiculos;
    }
    
    public void setTipo(String tip){
        this.tipoV = tip;
    }
    public void setPlaca(String placa){
        this.placa = placa;
    }
    
    public void setColor(String color){
        this.color = color;
    }
    
    public void setTipoM(String tipoM){
        this.tipo_motor = tipoM;
    }
    
    public void setPrecio(Double precio){
        this.precio = precio;
    }
    
    public void setComb(String tipoComb){
        this.tipo_combustible = tipoComb;
    }
    
    public void setRecorrido(double recorrido){
        this.recorrido = recorrido;
    }
    
    public void setAño(int año){
        this.año = año;
    }
    
    public void setListaVehic(ArrayList<Vehiculo> vehiculos){
        this.vehiculos = vehiculos;
    }
    
    /*Método para leer el archivo y mostrar una lista de los vehículos*/
    public ArrayList<Vehiculo> leerRegistros(String archivo){
        ArrayList <Vehiculo> regVehiculo = new ArrayList<>();
        /*Leyendo el archivo*/
        try(Scanner sc = new Scanner(new File(archivo))){
            /*Por cada salto de línea que encuentre sigue leyendo el archivo*/
            while(sc.hasNextLine()){
                /*Obtengo cada uno de los elementos del vehículo al separarlos por las comas*/
                String lin=sc.nextLine().strip();
                if(lin.length()>3){
                    String[] palabras = lin.split(",");
                    System.out.println(palabras);
                    /*Recordando que cuando voy a tomar una palabra es String*/
                    Vehiculo c = new Vehiculo(palabras[0],palabras[1],palabras[2],palabras[3],palabras[4],palabras[5],Double.parseDouble(palabras[6]),palabras[7],Double.parseDouble(palabras[8]),Integer.parseInt(palabras[9]));
                    /*Añado el nuevo vehículo a la lista*/
                    regVehiculo.add(c); 
                }
          }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        /*Mostrar la lista de registros actualizada*/
        return regVehiculo;
    }
    
    public void guardarRegistro(String archivo)
    {
        try(FileWriter fw = new FileWriter(archivo, true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw)){
            out.println(this.toString());
        } catch (Exception e) {
            System.out.println("");
        }
    }
    
    @Override
    public boolean equals(Object O){
        if(O == null){
            return false;
        }
        
        else if (this == O){
            return true;
        }
        
        else if(this.getClass()!= O.getClass()){
            return false;
        }
        /*Comparando si dos vehículos son iguales por su placa*/
        Vehiculo veh = (Vehiculo)O;
        return veh.placa == this.placa;       
    }
    
    @Override
    public String toString(){
        return this.tipoV+","+ this.placa + "," + this.color + "," + this.tipo_motor + ","+this.marca + "," + this.modelo + ","  + this.precio+ "," + this.tipo_combustible  + "," + this.recorrido + "," + this.año;
    }

    
    
    
    
    
}
