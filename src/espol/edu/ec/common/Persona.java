/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

/**
 *
 * @author andyc
 */
public class Persona {
    private String nombre; 
    private String apellido;
    private String cedula;
    private String organizacion;
    public Persona(){
        
    }
    public Persona(String nom, String ape, String ced, String org){
        this.nombre = nom;
        this.apellido = ape;
        this.cedula= ced;
        this.organizacion = org;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public String getNombre(){
        return nombre;
    }
    public void setApellido(String ap){
        this.apellido = ap;
    }
    public String getApellido(){
        return apellido;
    }
    public void setCedula(String cedula){
        this.cedula = cedula;
    }
    public String getCedula(){
        return cedula;
    }
    public void setOrg(String org){
        this.organizacion = org;
    }
    public String getOrg(){
        return organizacion;
    }
    @Override
    public boolean equals(Object o){
        if (o == null){
            return false;
        }
        if ( this == o ){
            return true;
        }
        if (o != o.getClass() ){
            return false;
        }
        
        Persona p = (Persona)o;
        return cedula.equals(p.cedula);
    }
    @Override 
    public String toString (){
        return "Los datos de la persona son " + "\n" + "Nombre " + nombre +  "\n" + "Apellidos  " + apellido +  "\n" + "Cédula " + cedula +
                 "\n" + "Organización  " + organizacion ;
    }
    
    
}
