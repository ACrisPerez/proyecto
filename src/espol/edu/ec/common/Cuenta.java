/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigInteger;  
import java.nio.charset.StandardCharsets; 
import java.security.MessageDigest;  
import java.security.NoSuchAlgorithmException;  
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author andyc
 */
public class Cuenta extends Persona {
    private String usuario;
    private String correo;
    private String clave;
    private ArrayList<Cuenta> cuentas;
    
    public Cuenta(String nom, String ape, String ced, String org , String usu, String correo, String clave){
        super(nom, ape, ced,org);
        this.usuario = usu;
        this.correo = correo;
        this.clave = clave;
    }
    public Cuenta(String usu, String correo, String clave){
        this.usuario = usu;
        this.correo = correo;
        this.clave = clave;
    }
    
    public void setCorreo (String correo){
        this.correo = correo; 
    }
    public void setContraseña(String clave){
        this.clave = clave;
    }
    public void setUsuario(String usu){
        this.usuario = usu;
    }
    public String getUsuario (){
        return usuario;
    }
    public String getCorreo (){
        return correo;
    }
    public String getContraseña(){
        return clave;
    }
    public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }
    public void setCuentas(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }
    
    public static byte[] getSHA(String input) throws NoSuchAlgorithmException 
    {  
        // Static getInstance method is called with hashing SHA  
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        // digest() method called  
        // to calculate message digest of an input  
        // and return array of byte 
        return md.digest(input.getBytes(StandardCharsets.UTF_8));  
    } 
     public static String toHexString(byte[] hash) 
    { 
        // Convert byte array into signum representation  
        BigInteger number = new BigInteger(1, hash);  
        // Convert message digest into hex value  
        StringBuilder hexString = new StringBuilder(number.toString(16));  
        // Pad with leading zeros 
        while (hexString.length() < 32)  
        {  
            hexString.insert(0, '0');  
        }  
  
        return hexString.toString();  
    }
     
    @Override //Comparo con respecto a la cuenta y la contraseña 
    public boolean equals (Object o ) {
        if (o == null){
            return false;
        }
        if (o != o.getClass() ){
            return false;
        }
        if ( this == o ){
            return true;
        }
        Cuenta c = (Cuenta)o;
        return correo.equals(c.correo) && clave.equals(c.clave) && usuario.equals(c.usuario);
    }
    
    public static ArrayList<Cuenta> leerRegistros(String archivo){
        ArrayList <Cuenta> regCuentas = new ArrayList<>();
        //Leer archivo
        try(Scanner sc = new Scanner(new File(archivo))){
            //Mientras que tenga una siguiente línea
            while(sc.hasNextLine()){
                //Obtengo la palabra es decir voy leyendo escogiendo cada elemento que esta separado por la coma
                String[] palabras = sc.nextLine().split(",");
                //Recordando que cuando voy a tomar una palabra es String
                //Agg el registro que lo paso aInteger ya que (cod, matricula(String), cod de materia)
                Cuenta c = new Cuenta(palabras[0],palabras[1],palabras[2]);
                //Añado a la lista 
                regCuentas.add(c);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        //Retorna la lista con los registros leidos
        return regCuentas;
    }
    public void guardarRegistro(String archivo){
        //Abro archivo
        try(FileWriter fw = new FileWriter(archivo, true);
        //Lo guardo en la memoria
        BufferedWriter bw = new BufferedWriter(fw);
        //Lo escribo bajo el formato del toString
        PrintWriter out = new PrintWriter(bw)){
            out.println(this.toString());
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    @Override
    public String toString(){
            return this.usuario + "," + this.correo + "," + this.clave;
        
    }
    
    //Retorna falso en caso de que el usuario sale repetido
    public static boolean validarUsuario(String usu){
        boolean usuValido = false;
        ArrayList<Cuenta> cta = Cuenta.leerRegistros("cuentas.txt");
        for(Cuenta c: cta){
            if(c.getUsuario().contains(usu)){
                return usuValido = true ; //En caso de que se encuentre vuelve a ingresar 
            }
            else{
                return usuValido = false; //En caso de que se puede ingresar
            }
        }
        return usuValido;
    }
    public static boolean validarCorreo(String correo){
        ArrayList<Cuenta> cta = Cuenta.leerRegistros("cuentas.txt");
        boolean correoVal = false;
        for(Cuenta c: cta){
            if(c.getCorreo().contains(correo) ){
                return correoVal = true ; //En caso de que se encuentre vuelve a ingresar 
            }
            else{
                return correoVal = false; //En caso de que se puede ingresar
            }
        }
        return correoVal;
    }
    
      public static boolean validarClave(String clave){
        ArrayList<Cuenta> cta = Cuenta.leerRegistros("cuentas.txt");
        boolean claveVal = false;
        for(Cuenta c: cta){
            if(c.getContraseña().contains(clave) ){
                return claveVal = true ; //En caso de que se encuentre vuelve a ingresar 
            }
            else{
                return claveVal = false; //En caso de que se puede ingresar
            }
        }
        return claveVal;
    }
    
    public static String generarContraseña(String clave){
       String conagg = null;
        try 
        { 
            System.out.println("***Generada exitosamente la contraseña***");
            conagg = toHexString(getSHA(clave));
             } 
        // For specifying wrong message digest algorithms  
        catch (NoSuchAlgorithmException e) {  
            System.out.println("Exception thrown for incorrect algorithm: " + e);  
        }  
       return conagg;
    }
    
    //En el mainnnn para cuenta 
       // Cuenta c1 = new Cuenta("Andrea","andycrisperez@outlook.com","hola123");
       // c1.guardarRegistro("cuentas.txt");
       /* String usu = null;
        String usagg = null;//El usuario para agg 
        System.out.println("***CUENTA***");
        Scanner sc=new Scanner(System.in);
        sc.useDelimiter("\n");
        sc.useLocale(Locale.US);
        ArrayList<Cuenta> cta = Cuenta.leerRegistros("cuentas.txt");
        //Ingreso el usuario
        do{
        System.out.print(" Ingrese el usuario: ");
        usu = sc.next().toLowerCase();//Confirmo que no sea repetido
        for(Cuenta c: cta){
            if(c.getUsuario().contains(usu)){
                System.out.println(" El ususario ya ha sido registrado, Vuelva a ingresar: ");
                usagg = usu;
            }
        }
        }while(usu  == usagg);
        //Ahora la cuenta 
        String correo = null;
        String coagg = null;//El usuario para agg 
        do{
        System.out.print(" Ingrese el correo: ");
        correo = sc.next().toLowerCase();//Confirmo que no sea repetido
        for(Cuenta c: cta){
            if(c.getCorreo().contains(correo) ){
                System.out.println(" El correo ya ha sido registrado, Vuelva a ingresar: ");
                coagg = correo;
            }
        }
        }while(coagg == correo);
        
        String contraseña;
        String conagg = null;
        System.out.print(" Ingrese la contraseña : ");
        contraseña = sc.next();//Confirmo que no sea repetido
        try 
        { 
            System.out.println("***Generada exitosamente la cuenta***");
             conagg = toHexString(getSHA(contraseña));
             } 
        // For specifying wrong message digest algorithms  
        catch (NoSuchAlgorithmException e) {  
            System.out.println("Exception thrown for incorrect algorithm: " + e);  
        }  
        //Guardar el nuevo usuario 
        Cuenta cuenNue = new Cuenta (usu.toLowerCase(),correo.toLowerCase(), conagg);
        cuenNue.guardarRegistro("cuentas.txt");*/
        
}
     


  
     
  
    

