/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import espol.edu.ec.common.Vehiculo;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author andyc
 */
public class Oferta {
    private String placa;
    private String correo;
    private double valorOferta;
    public Oferta(){
        super();
    }
    
    public Oferta(String pla, String correo, double val){
        this.placa = pla;
        this.correo = correo;
        this.valorOferta = val;
    }
    public void setCorreo(String correo){
        this.correo = correo;
    }
    public void setPlaca (String placa){
        this.placa = placa;
    }
    public void setOferta(double oferta){
        this.valorOferta = oferta;
    }
    public String getCorreo(){
        return correo;
    }
    public double getOferta(){
        return valorOferta;
    }
    public String getPlaca (){
        return placa;
    }
    
    public static ArrayList<Oferta> leerOfertas(String archivo){
        ArrayList<Oferta> oferta = new ArrayList<>();
        try(Scanner sc = new Scanner(new File(archivo))){
            while(sc.hasNextLine()){
                String[] palabras = sc.nextLine().split(",");
                Oferta o = new Oferta(palabras[0],palabras[1],Double.parseDouble(palabras[2]));
                oferta.add(o);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return oferta;
    }
    
    
    public void guardarOferta(String archivo){
        //Abro archivo
        try(FileWriter fw = new FileWriter(archivo, true);
        //Lo guardo en la memoria
        BufferedWriter bw = new BufferedWriter(fw);
        //Lo escribo bajo el formato del toString
        PrintWriter out = new PrintWriter(bw)){
            out.println(this.toString());
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    
    }
    
    @Override
    public String toString(){
        return this.placa +","+ this.correo +","+ this.valorOferta;        
    }
   
    
    
}
