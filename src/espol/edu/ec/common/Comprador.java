/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andyc
 */
public class Comprador extends Persona {
    
    public Comprador(){
        super();
    }
    public static ArrayList<Vehiculo> ofertarVehiculo(String tip,double reci, double recf,int añoi, int añof ,double preci, double precf){
        //Creo lista donde los voy a colocar    
        ArrayList<Vehiculo> vehiculosPrint = new ArrayList<>();
        //Abró el archivo en donde está guaradado y lo coloco en una lista
        ArrayList <Vehiculo> veh = leerRegistrosV("vehiculos.txt");
        //Recorró la lista donde se encuentra para buscar 
        for (Vehiculo v: veh){
            //Consulto si lo contiene en la lista
            if (v.getTipo().contains(tip) || (reci <= v.getRecorrido() && recf >= v.getRecorrido()) ||  (añoi <= v.getAño() && añof >= v.getAño()) || ( preci <= v.getPrecio() && precf >= v.getPrecio())){
                //Lo aggrego en una lista para que solo conserve lo de ese tipo
                vehiculosPrint.add(v);
                //Recorro la lista para buscar 
                }
            }
        return vehiculosPrint;
        
    }
    
    
   
   public static ArrayList<Vehiculo> leerRegistrosV(String archivo){
        ArrayList <Vehiculo> mVehiculo = new ArrayList<>();
        //Leer archivo
        try(Scanner sc = new Scanner(new File(archivo))){
            //Mientras que tenga una siguiente línea
            while(sc.hasNextLine()){
                //Obtengo la palabra es decir voy leyendo escogiendo cada elemento que esta separado por la coma
                String[] palabras = sc.nextLine().split(",");
                //Recordando que cuando voy a tomar una palabra es String
                //Agg el registro que lo paso aInteger ya que (cod, matricula(String), cod de materia)
                Vehiculo v = new Vehiculo(palabras[0],palabras[1],palabras[2],palabras[3],palabras[4],palabras[5], Double.parseDouble(palabras[6]),palabras[7],Double.parseDouble(palabras[8]),Integer.parseInt(palabras[9]));
                //Añado a la lista 
                mVehiculo.add(v);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        //Retorna la lista con los registros leidos
        return mVehiculo;
        
        
        //Comprador main
        //Viene de persona
        //Ofertar Vehículo
            /*String tipo =  null;
            String tipagg = null;
            double recorI = 0;
            double recorF =0;
            int añoI = 0;
            int añoF = 0;
            double precioI = 0;
            double precioF = 0;
            Scanner sc= new Scanner (System.in);
            sc.useDelimiter("\n");
            do{
            ArrayList <Vehiculo> tipV = new ArrayList<>();    
            System.out.print(" Ingrese el tipo de vehículo: ");
            tipo = sc.next();
            for(Vehiculo vT: tipV){
                if( !vT.getTipo().contains(tipo)){
                   System.out.println("El tipo de vehículo que ingreso no se encuentra ");
                   tipagg = null;
                }                
            }
            }while(tipagg == null);*/
        
    }
   
   //Menú de comprador 
    //Ofertar Vehículo
    //Ingresar los datos que desean
    /*ArrayList<Vehiculo> vehi = new ArrayList<>();
    ArrayList <Vehiculo> veh = leerRegistrosV("vehiculos.txt");
    Scanner sc = new Scanner(System.in);
    sc.useDelimiter("\n");
    String tipo = null;
    System.out.println("Realizar busquedad: " + "\n"  + "Seleccione el criterío que desea ingresar: " +"\n" + "1.Tipo de Vehículo" + 
            "\n" + "2.Recorrido" + "\n" + "3.Año" + "\n" + "4.Precio" + "\n" + "5.Regresar");
    System.out.println("Ingrese la opción: ");
    String opc_Comp = sc.next();
    while (! opc_Comp.equals("5") ){
        if (opc_Comp.equals("1")){
            do{
            System.out.println(" Ingrese el tipo de vehículo: ");
            tipo = sc.next();
            }while(!tipo.equals("Auto") || !tipo.equals("Camion") || !tipo.equals("Camioneta") || !tipo.equals("Motocicleta"));   
            System.out.println(" Regresar " );
        }
    }*/
    //Comprador c= new Comprador();
    //c.ofertarVehiculo("Auto", 0, 0, 2010, 2020, 0, 0);
}
