/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andyc
 */
public class Vendedor extends Persona{
    public Vendedor(){
        super();
    }
    
    public void registrarVendedor (String archivo){
        try(FileWriter fw = new FileWriter(archivo, true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw)){
            out.println(this.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    /*Método para devolver una lista con las ofertas realizadas al vehículo cuya placa se busca*/
    
    public ArrayList<Oferta> obtenerPorPlaca(String placa){
        //Leer archivo
        /*Estructura del archivo "Placa, correo del comprador, precio ofertado, precio del vehículo"*/
        ArrayList<Oferta> ofertas = new ArrayList<>();
        
        try(Scanner sc = new Scanner("oferta.txt")){            
            //Mientras que tenga una siguiente línea            
           // Vehiculo veh = new Vehiculo(); 
           // ArrayList<Vehiculo> listv = veh.leerRegistros("vehiculos.txt");
            ArrayList<Oferta> listO = Oferta.leerOfertas("ofertas.txt");
                //for(int i = 0; i<listv.size();i++){
             //       for(Vehiculo vehi: listv){
                        for(Oferta ofer: listO){
                            if(ofer.getPlaca().toUpperCase().equals(placa.toUpperCase())){
                            ofertas.add(ofer); 
                            }
                            else{
                                ofertas.isEmpty();
                            }
                        }
                    }            
        
                catch(Exception e){
                    System.out.println(e.getMessage());
                }
                return ofertas;    
            }
}
