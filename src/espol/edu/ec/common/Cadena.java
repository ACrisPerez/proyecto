/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;
import static java.lang.Double.parseDouble;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author F. Lopez
 */
public class Cadena {
    String cade;
    public Cadena(String s){
        cade=s.strip();
    }

    public boolean isdigit(){
        int c=0;
        for(int i=0;i<cade.length();i++){
            char let=cade.charAt(i);
            if(let==',')
                let='.';
            if (let!='1' && let!='2' && let!='3' && let!='4' && let!='5' && let!='6' && let!='7' && let!='8' && let!='9' && let!='0' && let!='.'){
                c++;
            }
        }
        if(c>0)
            return false;
        else
            return true;
    }
    
    public String title(){
        StringBuilder sb=new StringBuilder();
        int c=0;        
        for(int i=0;i<cade.length();i++){
            String let=String.valueOf(cade.charAt(i));
            if(i==0)
                sb.append(let.toUpperCase());
            else
                sb.append(let.toLowerCase());                              
        }
        return sb.toString();        
    }
    public boolean isvacio(){
        return cade.length()==0;
    }
    /*public void val_len(){
        Scanner ing=new Scanner(System.in);
        ing.useDelimiter("\n");
        ing.useLocale(Locale.US);
        while(cade.length()<3){
            System.out.println("Ingrese una cadena valida: ");
            cade=ing.next();
        }
        ing.close();          
    }*/
    public boolean valintervalo(){
        if(!cade.contains("-"))
            return false;
        int divi=cade.indexOf("-");
        String p1=cade.substring(0, divi);        
        String p2=cade.substring(divi+1, cade.length());
        Cadena c1=new Cadena(p1);
        Cadena c2=new Cadena(p2);
        if(!c1.isdigit() || !c2.isdigit()){
            System.out.println("Error en los intervalos");
            return false;
        }
        else
            return true;

    }
    
    @Override
    public String toString(){
        return cade;
    }
}
