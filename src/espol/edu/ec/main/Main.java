/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.main;
import espol.edu.ec.common.Mail;
import espol.edu.ec.common.Cadena;
import espol.edu.ec.common.Auto;
import espol.edu.ec.common.Camion;
import espol.edu.ec.common.Camioneta;
import espol.edu.ec.common.Cuenta;
import espol.edu.ec.common.Comprador;
import static espol.edu.ec.common.Cuenta.getSHA;
import static espol.edu.ec.common.Cuenta.toHexString;
import espol.edu.ec.common.Motocicleta;
import espol.edu.ec.common.Oferta;
import espol.edu.ec.common.Vehiculo;
import espol.edu.ec.common.Vendedor;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 *
 * @author andyc
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ent = new Scanner(System.in);
        ent.useDelimiter("\n");
        ent.useLocale(Locale.US);
        System.out.println("\nMenú de Principal de Opciones:\n\t1. Vendedor\n\t2. Comprador\n\t3.Salir");
        System.out.println("Ingrese opcion: ");        
        String opc_princ=ent.next();
        
        while(!opc_princ.equals("3")){
            if(opc_princ.equals("1")){
                System.out.println("\n               Menu del Vendedor");
                System.out.println("Menú de Opciones:\n\t1. Registrar un nuevo vendedor\n\t2. Ingresar un nuevo vehículo\n\t3. Aceptar oferta\n\t4. Salir");
                System.out.println("Ingrese una opcion: ");
                String opc_vend=ent.next();
                while(!opc_vend.equals("4")){//Menu del vendedor              
                    if(opc_vend.equals("1")){//Registro de nuevo vendedor
                        String nomb_vend;
                        String ape_vend;
                        String email;
                        System.out.println("Ingrese el nombre del vendedor: ");
                        
                        nomb_vend=ent.next().strip();
                        while(nomb_vend.length()<3){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            nomb_vend=ent.next().strip();
                        }                        
                        
                        System.out.println("Ingrese el apellido del vendedor: ");
                        ape_vend=ent.next().strip();
                        while(ape_vend.length()<3){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            ape_vend=ent.next().strip();
                        }
                        
                        //Validar email  
                        System.out.println("Ingrese el email del vendedor: ");
                        email=ent.next().strip();
                        while(email.length()<3 || !email.contains("@")){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            email=ent.next().strip();
                        }
                        
                        boolean val_email=Cuenta.validarCorreo(email); 
                        //boolean val_email=false;//Usar funcion que valide si el mail esta registrado
                        String salir="si";
                        while (val_email){
                            System.out.println("Correo ya registrado");
                            System.out.println("Ingrese otro correo o escriba Salir: ");
                            email=ent.next().strip();
                            if (email.toUpperCase().equals("SALIR")){
                                val_email=false;
                                salir="Salir";
                            }
                            else
                                email=ent.next().strip();
                                while(email.length()<3 || !email.contains("@")){
                                    System.out.println("Ingreso no valido, ingrese otra vez");
                                    email=ent.next().strip();
                                }
                                //boolean val_email=validaemail(email); 
                                val_email=val_email;//Llamar la funcion que valida si el mail esta registrado
                        }   
                        if(!salir.equals("Salir")){
                            String org;
                            String usuario;
                            
                            String Clave;
                            System.out.println("Ingrese la organizacion a la que pertenece el vendedor: ");
                            org=ent.next().strip();
                            while(org.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                org=ent.next().strip();
                            }
                            
                            
                            //Validar Usuario
                            System.out.println("Ingrese el usuario para el vendedor: ");
                            usuario=ent.next().strip();
                            while(usuario.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                usuario=ent.next().strip();
                            }
                            
                            //boolean val_usu=validausuario(usuario); 
                            boolean val_usu=false;//Usar funcion que valide si el usuario esta registrado
                            String sal="si";
                            while (val_usu){
                                System.out.println("Usuario ya registrado");
                                System.out.println("Ingrese otro usuario o escriba Salir: ");
                                usuario=ent.next().strip();
                                if (usuario.toUpperCase().equals("SALIR")){
                                    val_usu=false;
                                    sal="Salir";
                                }
                                else
                                    usuario=ent.next();
                                    //boolean val_usu=validausuario(usuario); 
                                    val_usu=val_usu;//Llamar la funcion que valida si el mail esta registrado
                            }
                            if(!sal.equals("Salir")){
                                String clave;
                                System.out.println("Ingrese la clave para el usuario: ");
                                clave=ent.next().strip();
                                //Llamar a la funcion hash
                                //Llamar al constructor de vendedor y guardarlo  Cuenta(String usu, String correo, String clave)
                                Cuenta cv=new Cuenta(usuario,email,Cuenta.generarContraseña(clave));
                                cv.guardarRegistro("cuentas.txt");
                            }
                            
                        }
                    }
                    else if(opc_vend.equals("2")){//Ingreso de vehiculo
                        String placa;                        
                        String modelo;
                        String tipo_vehiculo;
                        String tipo_motor;
                        int año;
                        double recorrido;
                        String color;
                        String tipo_comb;
                        int vidrios;
                        String transmision;
                        double precio;
                        String marca;
                        
                        System.out.println("Ingrese placa: ");
                        placa=ent.next().strip().toUpperCase();
                        while(!placa.contains("-")){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                placa=ent.next().strip();
                            }
                        //Validar placa
                        //boolean val_placa=validaplaca(placa);
                        boolean val_placa=false;
                        String sal="si";
                        while(val_placa){
                            System.out.println("Placa ya registrada");
                            System.out.println("Ingrese otro usuario o escriba Salir: ");
                            placa=ent.next().strip().toUpperCase();
                            if (placa.toUpperCase().equals("SALIR")){
                                val_placa=false;
                                sal="Salir";
                            }
                            else
                                placa=ent.next().strip().toUpperCase();
                                //boolean val_placa=validaplaca(placa);
                                val_placa=val_placa;//Llamar la funcion que valida si el mail esta registrado
                            
                        }
                        if(!sal.equals("Salir")){
                            System.out.println("Ingrese marca: ");
                            marca=ent.next().strip();
                            while(marca.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                marca=ent.next().strip();
                            } 
                            
                            
                            System.out.println("Ingrese modelo: ");
                            modelo=ent.next().strip();
                            while(modelo.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                modelo=ent.next().strip();
                            }

                            System.out.println("Ingrese el tipo de vehiculo: ");
                            tipo_vehiculo=ent.next().toUpperCase().strip();
                            while (!tipo_vehiculo.equals("MOTOCICLETA") && !tipo_vehiculo.equals("AUTO") && !tipo_vehiculo.equals("CAMION") && !tipo_vehiculo.equals("CAMIONETA")){
                                System.out.println("Ingrese un tipo de vehiculo valido: ");
                                tipo_vehiculo=ent.next().toUpperCase();
                            }

                            System.out.println("Ingrese tipo de motor: ");
                            tipo_motor=ent.next().strip();
                            
                            System.out.println("Ingrese año: ");
                            String anio;                            
                            anio=ent.next();
                            Cadena ca=new Cadena(anio);
                            while(!ca.isdigit()){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                anio=ent.next();
                                ca=new Cadena(anio);
                            }
                            año=parseInt(anio);

                            System.out.println("Ingrese recorrido: ");
                            
                            String recor=ent.next();
                            Cadena cr=new Cadena(recor);
                            while(!cr.isdigit()){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                recor=ent.next();
                                cr=new Cadena(recor);
                            }
                            recorrido=parseDouble(recor);
                            

                            System.out.println("Ingrese color: ");
                            color=ent.next().strip();
                            while(color.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                color=ent.next().strip();
                            }

                            System.out.println("Ingrese tipo combustible: ");
                            tipo_comb=ent.next().strip();
                            while(tipo_comb.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                tipo_comb=ent.next().strip();
                            }
                            
                            System.out.println("Ingrese el precio: ");
                            
                            String prec=ent.next();
                            Cadena cp=new Cadena(prec);
                            while(!cp.isdigit()){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                prec=ent.next();
                                cp=new Cadena(prec);
                            }
                            precio=parseDouble(prec);                          
                                                    
                            if(!tipo_vehiculo.equals("MOTOCICLETA")){
                                System.out.println("Ingrese el tipo de transmision: ");
                                transmision=ent.next().strip();
                                while(transmision.length()<3){
                                    System.out.println("Ingreso no valido, ingrese otra vez");
                                    transmision=ent.next().strip();
                                }  
                                
                                System.out.println("Ingrese la cantidad de vidrios que tiene el vehiculo: ");                                
                                String vid;                            
                                vid=ent.next();
                                Cadena cv=new Cadena(vid);
                                while(!cv.isdigit()){
                                    System.out.println("Ingreso no valido, ingrese otra vez");
                                    vid=ent.next();
                                    cv=new Cadena(anio);
                                }
                                vidrios=parseInt(vid);

                                if(tipo_vehiculo.equals("AUTO")){
                                    //Llamar y guardar al constructor de auto
                                    
                                    Vehiculo au = new Auto(tipo_vehiculo,placa, color, tipo_motor, marca,  modelo,  precio, tipo_comb, recorrido, año, transmision, vidrios);
                                    /*Guardando el vehículo en el documento*/
                                    au.guardarRegistro("vehiculos.txt");
                                    /*MOSTRANDO LA LISTA DE VEHÍCULOS ACTUALIZADA*/
                                    System.out.println("El auto fue registrado exitosamente");
                 
                                }
                                else if (tipo_vehiculo.equals("CAMIONETA")){
                                    //Llamar y guardar al constructor de camioneta                                
                                    
                                    Vehiculo cami = new Camioneta(tipo_vehiculo,placa, color, tipo_motor, marca,  modelo,  precio, tipo_comb, recorrido, año, transmision, vidrios);
                                    /*Guardando el vehículo en el documento*/
                                    cami.guardarRegistro("vehiculos.txt");
                                    /*MOSTRANDO LA LISTA DE VEHÍCULOS ACTUALIZADA*/
                                    System.out.println("La camioneta fue registrada exitosamente");
                                   
                                }
                                else if (tipo_vehiculo.equals("CAMION")){
                                    //Llamar y guardar al constructor de camion                              
                                    
                                    Vehiculo Cam = new Camion(tipo_vehiculo,placa, color, tipo_motor, marca,  modelo,  precio, tipo_comb, recorrido, año, transmision, vidrios);
                                    /*Guardando el vehículo en el documento*/
                                    Cam.guardarRegistro("vehiculos.txt");
                                    /*MOSTRANDO LA LISTA DE VEHÍCULOS ACTUALIZADA*/
                                    System.out.println("El camion fue registrado exitosamente");

                                }                                
                            }
                            else{
                                //Llamar y guardar el constructor de la moto
                                Vehiculo mo = new Motocicleta(tipo_vehiculo,placa, color, tipo_motor, marca,  modelo,  precio, tipo_comb, recorrido, año);
                                /*Guardando el vehículo en el documento*/
                                mo.guardarRegistro("vehiculos.txt");
                                /*MOSTRANDO LA LISTA DE VEHÍCULOS ACTUALIZADA*/
                                System.out.println("La motocicleta fue registrada exitosamente");
                            }
                        }                    
                           
                    }
                    else if(opc_vend.equals("3")){//Aceptar Oferta
                        String usu;
                        String clave;
                        //Consulta usuario
                        System.out.println("Ingrese usuario del vendedor: ");
                        usu=ent.next().strip();
                        boolean val_usu=true;//Usar funcion que valida si el usuario esta
                        //boolean val_usu=validausuario(usu);
                        String salida="";
                        while (!val_usu){
                            System.out.println("Usuario no encontrado, ingrese otro o ingrese Salir: ");
                            usu=ent.next().strip();
                            if(usu.toUpperCase().equals("SALIR")){
                                val_usu=true;
                                salida="Salida";
                            }
                            else{
                                //boolean val_usu=validausuario(usu);
                                val_usu=val_usu;//Usar funcion que valida si es uuario esta
                                
                            }
                        }                      
                        if(!salida.equals("Salida")){
                            //Consulta clave
                            clave=ent.next().strip();
                            boolean val_clav=true;//Usar funcion que valida si la clave es correcta
                            //boolean val_clav=validaclave(clave);
                            String sali="";
                            while (!val_clav){
                                System.out.println("Clave Incorrecta, intente de nuevo o ingrese Salir: ");
                                clave=ent.next().strip();
                                if(clave.toUpperCase().equals("SALIR")){
                                    val_usu=true;
                                    sali="Salida";
                                }
                                else{
                                    //boolean val_clav=validaclave(clave);
                                    val_clav=val_clav;//Usar funcion que valida si la clave es correcta
                                }
                            }   
                            if(!sali.equals("Salida")){
                                //INICIAMOS LA SESION Y PODEMOS ACEPTAR OFERTAS
                                String placa;
                                System.out.println("Ingrese placa del vehiculo del cual desea buscar ofertas: ");
                                placa=ent.next().strip().toUpperCase();
                                String sal="si";
                                boolean val_placa=true;//Llamar a la funcion que indica si la placa esta entre las registradas
                                //b
                                while(!val_placa){
                                    System.out.println("Placa no encontrada");
                                    System.out.println("Ingrese otra placa o escriba Salir: ");
                                    placa=ent.next().strip().toUpperCase();
                                    if (placa.toUpperCase().equals("SALIR")){
                                        val_placa=true;
                                        sal="Salir";
                                    }
                                    else
                                        placa=ent.next().strip().toUpperCase();
                                        //boolean val_placa=validaplaca(placa);
                                        val_placa=val_placa;//Llamar la funcion que valida si el mail esta registrado

                                }
                                if(!sal.equals("Salir")){
                                    //Mostramos las ofertas
                                    //Posible funcionamiento del programa
                                    //Usando iterator
                                    ArrayList<Integer> lista_ind=new ArrayList<>();
                                    Vendedor vend= new Vendedor();                                    
                                    ArrayList<Oferta> ofertas = vend.obtenerPorPlaca(placa);
                                    for(int i=1;i<ofertas.size()+1;i++){
                                        lista_ind.add(i);
                                    }
                                    

                                    if(ofertas.size()>0){
                                        //ArrayList<int> lista_ind = new ArrayList<int>() //Crear el arrego de indices paralelo a las ofertas
                                        ListIterator<Oferta> iter_ofer = ofertas.listIterator();
                                        ListIterator<Integer> iter_ind = lista_ind.listIterator();
                                        String opc="1";  
                                        String menu="\nIngrese\n1. Revisar la oferta siguiente\n2 Revisar la oferta anterior\n3. Aceptar oferta\n4. Salir";
                                        Oferta ofer=null;
                                        int ind_ofer;
                                        while(!opc.equals("3") && !opc.equals("4")){                                            
                                            if(opc.equals("1")){
                                                if(iter_ofer.hasNext()){
                                                    ofer=iter_ofer.next();
                                                    ind_ofer=iter_ind.next();
                                                    System.out.println("Oferta "+ind_ofer);
                                                    System.out.println(ofer.toString());                                                    
                                                    System.out.println(menu);
                                                    System.out.println("Usted ingresa: ");                                        
                                                    opc=ent.next().strip();
                                                }
                                                else{
                                                    System.out.println("Error, no existen mas ofertas por revisar"); 
                                                    System.out.println("\nIngrese\n2 Revisar la oferta anterior\n3 Aceptar oferta\n4. Salir");                                                    
                                                    System.out.println("Usted ingresa: ");                                        
                                                    opc=ent.next().strip();
                                                }
                                            }
                                            else if(opc.equals("2")){
                                                if(iter_ofer.hasPrevious()){
                                                    ofer=iter_ofer.previous();
                                                    ind_ofer=iter_ind.previous();
                                                    System.out.println("Oferta "+ind_ofer);
                                                    System.out.println(ofer.toString());
                                                    System.out.println(menu);
                                                    System.out.println("Usted ingresa: ");                                        
                                                    opc=ent.next().strip();
                                                }
                                                else{
                                                    System.out.println("Error, no existen ofertas anteriores"); 
                                                    System.out.println("\nIngrese\n1. Revisar la oferta siguiente\n3. Aceptar la oferta\n4. Salir");                                                    
                                                    System.out.println("Usted ingresa: ");                                        
                                                    opc=ent.next().strip();
                                               }
                                            }
                                            else{
                                                System.out.println("Ingreso no valido"); 
                                                System.out.println(menu);                                                    
                                                System.out.println("Usted ingresa: ");                                        
                                                opc=ent.next().strip();
                                            }
                                        }
                                        if(opc.equals("3")){
                                            System.out.println("Oferta Aceptada");
                                            //ENVIAR EL CORREO DE QUE SE ACEPTO LA OFERTA PILAAAAS 
                                            Mail.enviarcorreo(ofer.getCorreo(),"Felicidades la oferta que realizo por el vehiculo de placa "+ofer.getPlaca()+" fue aceptada exitosamente");
                                            iter_ofer.remove();
                                            iter_ind.remove();
                                            System.out.println();
                                            //ListArray ofertodas=Oferta.leerOfertas("");
                                            ///////////////////////////////////////
                                            //////////////
                                            
                                            ArrayList<Oferta> arrayOfertas = Oferta.leerOfertas("ofertas.txt");
                                            for(Oferta of: arrayOfertas){
                                                if (of.getPlaca().equals(placa))
                                                    arrayOfertas.remove(of);                                                
                                            
                                                    }
                                            for(Oferta of: arrayOfertas){                                                
                                                    of.guardarOferta("Ofertas.txt");                                               
                                            
                                                    }

                                        }
                                    }
                                    else
                                        System.out.println("La placa "+placa+" no tiene ofertas");                                    
                                }    
                            }   
                        }     
                    }
                    else
                        System.out.println("Error de Ingreso");
                    
                    System.out.println("\n\tMenu del Vendedor");
                    System.out.println("Menú de Opciones:\n\t1. Registrar un nuevo vendedor\n\t2. Ingresar un nuevo vehículo\n\t3. Aceptar oferta\n\t4. Salir");
                    System.out.println("Ingrese una opcion: ");
                    opc_vend=ent.next().strip();                
                }
            }

            else if(opc_princ.equals("2")){//Comprador
                String opc_comp;
                String menu_comp="\nMenu del Comprador\n\t1. Registrar un nuevo Comprador\n\t2. Ofertar por un vehiculo\n\t3. Salir";                
                opc_comp="";
                
                while(!opc_comp.equals("3")){
                    System.out.println(menu_comp);
                    System.out.println("Ingrese una opcion del menu");
                    opc_comp=ent.next().strip();
                    if(opc_comp.equals("1")){
                        String nomb_comp;
                        String ape_comp;
                        String email;
                        System.out.println("Ingrese el nombre del comprador: ");
                        
                        nomb_comp=ent.next().strip();
                        while(nomb_comp.length()<3){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            nomb_comp=ent.next().strip();
                        }                        
                        
                        System.out.println("Ingrese el apellido del comprador: ");
                        ape_comp=ent.next().strip();
                        while(ape_comp.length()<3){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            ape_comp=ent.next().strip();
                        }
                        
                        //Validar email  
                        System.out.println("Ingrese el email del comprador: ");
                        email=ent.next().strip();
                        while(email.length()<3 || !email.contains("@")){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            email=ent.next().strip();
                        }
                        
                        //boolean val_email=validaemail(email); 
                        boolean val_email=false;//Usar funcion que valide si el mail esta registrado
                        String salir="si";
                        while (val_email){
                            System.out.println("Correo ya registrado");
                            System.out.println("Ingrese otro correo o escriba Salir: ");
                            email=ent.next().strip();
                            if (email.toUpperCase().equals("SALIR")){
                                val_email=false;
                                salir="Salir";
                            }
                            else
                                email=ent.next().strip();
                                //boolean val_email=validaemail(email); 
                                val_email=val_email;//Llamar la funcion que valida si el mail esta registrado
                        }   
                        if(!salir.equals("Salir")){
                            String org;
                            String usuario;
                            
                            String Clave;
                            System.out.println("Ingrese la organizacion a la que pertenece el comprador: ");
                            org=ent.next().strip();
                            while(org.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                org=ent.next().strip();
                            }
                            
                            
                            //Validar Usuario
                            System.out.println("Ingrese el usuario para el comprador: ");
                            usuario=ent.next().strip();
                            while(usuario.length()<3){
                                System.out.println("Ingreso no valido, ingrese otra vez");
                                usuario=ent.next().strip();
                            }
                            
                            //boolean val_usu=validausuario(usuario); 
                            boolean val_usu=false;//Usar funcion que valide si el usuario esta registrado
                            String sal="si";
                            while (val_usu){
                                System.out.println("Usuario ya registrado");
                                System.out.println("Ingrese otro usuario o escriba Salir: ");
                                usuario=ent.next().strip();
                                if (usuario.toUpperCase().equals("SALIR")){
                                    val_usu=false;
                                    sal="Salir";
                                }
                                else
                                    usuario=ent.next();
                                    //boolean val_usu=validausuario(usuario); 
                                    val_usu=val_usu;//Llamar la funcion que valida si el mail esta registrado
                            }
                            if(!sal.equals("Salir")){
                                String clave;
                                System.out.println("Ingrese la clave para el usuario: ");
                                clave=ent.next().strip(); 
                                //Llamar a la funcion hash
                                //Llamar al constructor de comprador y guardarlo                                
                            }  
                        }
                    }
                    else if(opc_comp.equals("2")){
                        //Generamos el arraylist de todos los vehiculos
                        //Definimos un arraylist que almacene a los vehiculos que esten cumpliendo con las condiciones
                        String tipo_vehiculo="";
                        String recorrido;
                        String anio;
                        String precio;
                        int anio_ini=0;
                        int anio_fin=0;
                        double prec_ini=0;
                        double prec_fin=0;
                        double rec_ini = 0;
                        double rec_fin=0;
                        System.out.println("Ingrese tipo del vehiculo: ");
                        tipo_vehiculo=ent.next().strip().toUpperCase();
                        while(!tipo_vehiculo.equals("") && !tipo_vehiculo.equals("MOTOCICLETA") && !tipo_vehiculo.equals("AUTO") && !tipo_vehiculo.equals("CAMION") && !tipo_vehiculo.equals("CAMIONETA")){
                            System.out.println("Error ingrese, ingrese de nuevo: ");
                            tipo_vehiculo=ent.next().strip().toUpperCase();
                        }
                        
                        
                        if(!tipo_vehiculo.isEmpty())
                            //Filtramos por tipo de vehiculo aqui
                            
                            
                        
                        System.out.println("Ingrese recorrido con el formato: recorrido1-recorrido2");
                        recorrido=ent.next();
                        Cadena cr=new Cadena(recorrido);
                        while(!cr.valintervalo() && !cr.isvacio()){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            recorrido=ent.next();
                            cr=new Cadena(recorrido);
                        }

                        if(!recorrido.isEmpty()){
                            int divi;
                            //Filtramos por el recorrido                            
                            divi=recorrido.indexOf("-");
                            String p1=recorrido.substring(0, divi);
                            String p2=recorrido.substring(divi+1, recorrido.length());
                            System.out.println(recorrido+" "+p1+" "+p2);
                            rec_ini=parseDouble(p1);
                            rec_fin=parseDouble(p2);
                            
                        }
                        
                        System.out.println("Ingrese año con el formato: año 1-año 2");
                        anio=ent.next();
                        Cadena ca=new Cadena(anio);
                        while(!ca.valintervalo() && !ca.isvacio()){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            anio=ent.next();
                            ca=new Cadena(anio);
                        }
                        if(!anio.isEmpty()){
                            int divi;
                            //Filtramos por el año                         
                            divi=anio.indexOf("-");
                            String p1=anio.substring(0, divi);
                            String p2=anio.substring(divi+1, anio.length());
                            anio_ini=parseInt(p1);
                            anio_fin=parseInt(p2);
                            
                        }
                        
                        
                        System.out.println("Ingrese precio con el formato: precio 1-precio 2");
                        precio=ent.next();
                        Cadena cp=new Cadena(precio);
                        while(!cp.valintervalo() && !cp.isvacio()){
                            System.out.println("Ingreso no valido, ingrese otra vez");
                            precio=ent.next();
                            cp=new Cadena(precio);
                        }
                        if(!precio.isEmpty()){
                            int divi;
                            //Filtramos por el año                         
                            divi=precio.indexOf('-');
                            System.out.println(divi);
                            String p1=precio.substring(0, divi);
                            String p2=precio.substring(divi+1, precio.length());
                            prec_ini=parseDouble(p1);
                            prec_fin=parseDouble(p2);
                        }
                        //HACER el filtrado de vehiculos por partes y no de golpe
              
   
                        ArrayList <Vehiculo> lista_veh = Comprador.ofertarVehiculo(tipo_vehiculo, rec_ini, rec_fin, anio_ini, anio_fin, prec_ini, prec_fin);
                                                
                        
                        //Iniciamos con las ofertas
                        //Mostramos las ofertas
                        //Posible funcionamiento del programa
                        //Usando iterator
                        
                        if(lista_veh.size()>0){
                             //Crear el arrego de indices paralelo a las ofertas
                            ArrayList<Integer> lista_ind=new ArrayList<>();                           
                            
                            for(int i=1;i<lista_veh.size()+1;i++){
                                lista_ind.add(i);
                            }
                            
                            ListIterator<Vehiculo> iter_veh = lista_veh.listIterator();
                            ListIterator<Integer> iter_ind = lista_ind.listIterator();
                            String opc="1";  
                            String menu="\nIngrese\n1. Revisar la oferta siguiente\n2 Revisar la oferta anterior\n3. Ofertar\n4. Salir";
                            Vehiculo vehi=null;
                            int ind_vehi=0;
                            while(!opc.equals("3") && !opc.equals("4")){                                            
                                if(opc.equals("1")){
                                    if(iter_veh.hasNext()){
                                        vehi=iter_veh.next();
                                        ind_vehi=iter_ind.next();
                                        System.out.println("Vehiculo "+ind_vehi);
                                        System.out.println(vehi.toString());                                                    
                                        System.out.println(menu);
                                        System.out.println("Usted ingresa: ");                                        
                                        opc=ent.next().strip();
                                    }
                                    else{
                                        System.out.println("Error, no existen mas ofertas por revisar"); 
                                        System.out.println("\nIngrese\n2 Revisar la oferta anterior\n3 Aceptar Oferta\n4. Salir");                                                    
                                        System.out.println("Usted ingresa: ");                                        
                                        opc=ent.next().strip();
                                    }
                                }
                                else if(opc.equals("2")){
                                    if(iter_veh.hasPrevious()){
                                        vehi=iter_veh.previous();
                                        ind_vehi=iter_ind.previous();
                                        System.out.println("Vehiculo "+ind_vehi);
                                        System.out.println(vehi.toString());
                                        System.out.println(menu);
                                        System.out.println("Usted ingresa: ");                                        
                                        opc=ent.next().strip();
                                        
                                    }
                                    else{
                                        System.out.println("Error, no existen ofertas anteriores"); 
                                        System.out.println("\nIngrese\n1. Revisar la oferta siguiente\n3. Ofertar\n4. Salir");                                                    
                                        System.out.println("Usted ingresa: ");                                        
                                        opc=ent.next().strip();
                                   }
                                }
                                else{
                                    System.out.println("Ingreso no valido"); 
                                    System.out.println(menu);                                                    
                                    System.out.println("Usted ingresa: ");                                        
                                    opc=ent.next().strip();
                                }
                            }
                            if(opc.equals("3") && ind_vehi!=0){
                                //Hacer oferta
                                System.out.println("Ingrese el precio con el que desea ofertar dicho vehiculo: "); 
                                String prec_ofer=ent.next();                                
                                Cadena cpo= new Cadena(prec_ofer);
                                
                                double prec_of=0;                                
                                
                                if(precio.strip().isEmpty()){
                                    boolean condi=false;
                                    if(cpo.isdigit())
                                        condi=parseDouble(prec_ofer)>0;
                                    while (!condi){
                                        System.out.println("Error de ingreso, ingrese de nuevo el precio: ");
                                        prec_ofer=ent.next();                                
                                        cpo= new Cadena(prec_ofer);
                                        if(cpo.isdigit())
                                            condi=parseDouble(prec_ofer)>0;
                                    }
                                    prec_of=parseDouble(prec_ofer);
                                }
                                else{
                                    
                                    boolean cond2=false;
                                    if(cpo.isdigit()){                                  
                                        int divi;                 
                                        divi=recorrido.indexOf("-");
                                        String p1=precio.substring(0, divi);
                                        String p2=precio.substring(divi+1, precio.length());
                                        prec_ini=parseDouble(p1);
                                        prec_fin=parseDouble(p2);
                                        cond2=parseDouble(prec_ofer)>=prec_ini && parseDouble(prec_ofer)<=prec_fin;
                                    }
                                    while (!cond2){
                                        System.out.println("Error de ingreso, ingrese de nuevo el precio: ");
                                        prec_ofer=ent.next();                                
                                        cpo= new Cadena(prec_ofer);
                                        if(cpo.isdigit()){                                  
                                            int divi;                 
                                            divi=recorrido.indexOf("-");
                                            String p1=precio.substring(0, divi);
                                            String p2=precio.substring(divi+1, precio.length());
                                            prec_ini=parseDouble(p1);
                                            prec_fin=parseDouble(p2);
                                            cond2=parseDouble(prec_ofer)>=prec_ini && parseDouble(prec_ofer)<=prec_fin;
                                        }
                                    }
                                    prec_of=parseDouble(prec_ofer);                              
                                }
                                
                                System.out.println("Ingrese su email para contactarnos con usted: ");
                                String email=ent.next().strip();
                                while(email.length()<3 || !email.contains("@")){
                                    System.out.println("Ingreso no valido, ingrese otra vez");
                                    email=ent.next().strip();
                                }
                                String placa=vehi.getPlaca();
                                Oferta oferta=new Oferta(placa,email,prec_of);
                                oferta.guardarOferta("ofertas.txt");  
                            } 
                        }
                    }
                    else
                        System.out.println("Error de Ingreso");
                }
            }
            else
                System.out.println("Error de Ingreso");            
            System.out.println("\nMenú de Principal de Opciones:\n\t1. Vendedor\n\t2. Comprador\n\t3. Salir");
            System.out.println("Ingrese opcion: ");
            opc_princ=ent.next();
            }
            
        
        ent.close();
        System.out.println("\nFin del Programa");
    }

}
